import React from "react";
import "../../../../css/leftside.css";
import Link from "next/link";
import Image from "next/image";
import fbIcon from "../../../../public/Facebook.png";
import InstaIcon from "../../../../public/Insta.png";
import YTBIcon from "../../../../public/Youtube.png";
import TumblrIcon from "../../../../public/tumblr.png";
import TwIcon from "../../../../public/twitter.png";
import CHPlay from "../../../../public/android1.png";
import AppStore from "../../../../public/appstore1.png";

export default function Leftside() {
  return (
    <div className="leftside col-span-2">
      <div className="frame">
        {/* logo */}
        <div className="logo mb-[10px]">
          <img
            src="https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/logo.png"
            alt="logo footer"
          />
        </div>
        {/* text */}
        <div className="text space-y-[5px]">
          <p>
            Lầu 5, 387-389 Hai Bà Trưng Quận 3 TP HCM
            <br />
            Công Ty Cổ Phần Phát Hành Sách TP HCM - FAHASA
            <br />
            60 - 62 Lê Lợi, Quận 1, TP. HCM, Việt Nam
          </p>
          <p>
            Fahasa.com nhận đặt hàng trực tuyến và giao hàng tận nơi. KHÔNG hỗ
            trợ đặt mua và nhận hàng trực tiếp tại văn phòng cũng như tất cả Hệ
            Thống Fahasa trên toàn quốc.
          </p>
        </div>
        {/* Bộ công thương */}
        <div className="w-28 py-4">
          <img
            src="https://cdn0.fahasa.com/media/wysiwyg/Logo-NCC/logo-bo-cong-thuong-da-thong-bao1.png"
            alt="Bộ công thương"
          />
        </div>
        {/* Mạng xã hội */}
        <div className="socialMedia flex">
          {/* facebook */}
          <Link href="#">
            <Image src={fbIcon} alt="FB" />
          </Link>
          {/* insta */}
          <Link href="#">
            <Image src={InstaIcon} alt="Insta" />
          </Link>
          {/* Youtube */}
          <Link href="#">
            <Image src={YTBIcon} alt="YTB" />
          </Link>
          {/* Tumblr*/}
          <Link href="#">
            <Image src={TumblrIcon} alt="Tumblr" />
          </Link>
          {/* Twitter*/}
          <Link href="#">
            <Image src={TwIcon} alt="Tw" />
          </Link>
        </div>
        {/* Download App */}
        <div className="app">
          <a href="#">
            <Image src={CHPlay} alt="CHPlay" />
          </a>
          <a href="#">
            <Image src={AppStore} alt="AppStore" />
          </a>
        </div>
      </div>
    </div>
  );
}
