import React from "react";
import "../../../css/footer.css";
import Leftside from "./Leftside/Leftside";
import Rightside from "./Rightside/Rightside";

export default function Footer() {
  return (
    <footer>
      <div className="container">
        <div className="grid grid-cols-6 ">
          {/* giao diện trái*/}
          <Leftside />
          {/* giao diện phải */}
          <Rightside />
          {/* Giấy chứng nhận đăng ký kinh doanh */}.
          <div class="businessRegistration col-span-6 p-4">
            <p className="text-center text-[#adadad]">
              Giấy chứng nhận Đăng ký Kinh doanh số 0304132047 do Sở Kế hoạch và
              Đầu tư Thành phố Hồ Chí Minh cấp ngày 20/12/2005, đăng ký thay đổi
              lần thứ 10, ngày 20/05/2022.
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}
