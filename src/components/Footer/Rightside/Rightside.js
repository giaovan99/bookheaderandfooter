import React from "react";
import "../../../../css/rightside.css";
import addressLogo from "../../../../public/addresLogo.png";
import mailIcon from "../../../../public/mailicon.png";
import phoneIcon from "../../../../public/phoneicon.png";
import Image from "next/image";

export default function Rightside() {
  return (
    <div className="rightside col-span-4 space-y-8">
      {/* Links liên kết các trang khác */}
      <div className="grid grid-cols-3">
        {/* item */}
        <div className="linkItem service space-y-4">
          <h3 className="title">Dịch vụ</h3>
          <div className="links">
            <ul className="space-y-4">
              <li>
                <a href="#">Điểu khoản sử dụng</a>
              </li>
              <li>
                <a href="#">Chính sách bảo mật thông tin cá nhân</a>
              </li>
              <li>
                <a href="#">Chính sách bảo mật thanh toán</a>
              </li>
              <li>
                <a href="#">Giới thiệu Fahasa</a>
              </li>
              <li>
                <a href="#">Hệ thống trung tâm nhà sách</a>
              </li>
            </ul>
          </div>
        </div>
        {/* item */}
        <div className="linkItem service space-y-4">
          <h3 className="title">Hỗ trợ</h3>
          <div className="links">
            <ul className="space-y-4">
              <li>
                <a href="#">Chính sách đổi - trả - hoàn tiền</a>
              </li>
              <li>
                <a href="#">Chính sách bảo hành - bồi hoàn</a>
              </li>
              <li>
                <a href="#">Chính sách vận chuyển</a>
              </li>
              <li>
                <a href="#">Chính sách khách sỉ</a>
              </li>
              <li>
                <a href="#">Phương thức thanh toán và xuất HĐ</a>
              </li>
            </ul>
          </div>
        </div>
        {/* item */}
        <div className="linkItem service space-y-4">
          <h3 className="title">TÀI KHOẢN CỦA TÔI</h3>
          <div className="links">
            <ul className="space-y-4">
              <li>
                <a href="#">Đăng nhập/Tạo mới tài khoản</a>
              </li>
              <li>
                <a href="#">Thay đổi địa chỉ khách hàng</a>
              </li>
              <li>
                <a href="#">Chi tiết tài khoản</a>
              </li>
              <li>
                <a href="#">Lịch sử mua hàng</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {/* Liên hệ */}
      <div className="grid grid-cols-3 gap-4">
        {/* title */}
        <div className="col-span-3">
          <h3 className="title">Liên hệ</h3>
        </div>
        {/* địa chỉ */}
        <div className="address flex items-center space-x-2">
          <Image src={addressLogo} alt="..." width={11} />
          <p>60-62 Lê Lợi, Q.1, TP. HCM</p>
        </div>
        {/* mail */}
        <div className="mail flex items-center space-x-2">
          <Image src={mailIcon} alt="..." width={18} />
          <a href="mailto:cskh@fahasa.com.vn">
            <p>cskh@fahasa.com.vn</p>
          </a>
        </div>
        {/* hotline */}
        <div className="hotline flex items-center space-x-2">
          <Image src={phoneIcon} alt="..." width={15} />
          <a href="tel:1900636467">
            <p>1900636467</p>
          </a>
        </div>
      </div>
      {/* Hình ảnh đơn vị vận chuyển */}
      <div className="grid grid-cols-5 shippingUnit gap-8 mr-4">
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media/wysiwyg/Logo-NCC/vnpost1.png"
            alt="..."
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media/wysiwyg/Logo-NCC/ahamove_logo3.png"
            alt="..."
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media/wysiwyg/Logo-NCC/icon_giao_hang_nhanh1.png"
            alt="..."
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media/wysiwyg/Logo-NCC/icon_snappy1.png"
            alt="..."
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media/wysiwyg/Logo-NCC/Logo_ninjavan.png"
            alt="..."
          />
        </div>
      </div>
      {/* Hình ảnh đơn vị thanh toán */}
      <div className="grid grid-cols-5 paymentUnit gap-8 mr-4">
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media//wysiwyg/Logo-NCC/vnpay_logo.png"
            alt="..."
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media//wysiwyg/Logo-NCC/ZaloPay-logo-130x83.png"
            alt="..."
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media//wysiwyg/Logo-NCC/momopay.png"
            alt="..."
            style={{ height: "50px" }}
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media//wysiwyg/Logo-NCC/shopeepay_logo.png"
            alt="..."
            style={{ height: "44px" }}
          />
        </div>
        {/* item */}
        <div className="item">
          <img
            src="https://cdn0.fahasa.com/media//wysiwyg/Logo-NCC/logo_moca_120.jpg"
            alt="..."
            style={{ height: "65px" }}
          />
        </div>
      </div>
    </div>
  );
}
