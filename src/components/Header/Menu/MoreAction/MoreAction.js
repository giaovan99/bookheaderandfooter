import React from "react";
import Notification from "./Notification/Notification";
import "../../../../../css/moreAction.css";
import Cart from "./Cart/Cart";
import Account from "./Account/Account";
import Language from "./Language/Language";
export default function MoreAction() {
  return (
    <div className="moreAction grow">
      {/* Thông báo */}
      <Notification />
      {/* Giỏ hàng */}
      <Cart />
      {/* Tài khoản */}
      <Account />
      {/* Ngôn ngữ */}
      <Language />
    </div>
  );
}
