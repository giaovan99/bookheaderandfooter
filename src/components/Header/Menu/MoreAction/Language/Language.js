import { Dropdown, Space } from "antd";
import React from "react";
import arrowDownIcon from "../../../../../../public/icon_seemore_gray.svg";
import VN from "../../../../../../public/VNFlag.svg";
import Eng from "../../../../../../public/EngFlag.svg";
import Image from "next/image";

export default function Language() {
  const items = [
    {
      label: (
        <div className="itemLanguage">
          <p className="flag">
            <Image src={VN} />
          </p>
          <p className="text">VN</p>
        </div>
      ),
      key: "0",
    },
    {
      label: (
        <div className="itemLanguage">
          <p className="flag">
            <Image src={Eng} />
          </p>
          <p className="text">EN</p>
        </div>
      ),
      key: "1",
    },
  ];
  return (
    <div className="language">
      <Dropdown
        menu={{
          items,
        }}
        trigger={["click"]}
      >
        <a onClick={(e) => e.preventDefault()}>
          <Space>
            <p className="languageActive">
              <Image src={VN} alt="flag" />
            </p>
            <p>
              <Image src={arrowDownIcon} alt="icon" />
            </p>
          </Space>
        </a>
      </Dropdown>
    </div>
  );
}
